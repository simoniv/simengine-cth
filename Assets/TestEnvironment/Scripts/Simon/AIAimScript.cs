



using UnityEngine;
using System.Collections;

public class AIAimScript : MonoBehaviour {
	
	
	//Whatever shall be shot
	public Rigidbody bullet;
	
	//Initial bullet velocity
	private float bulletVelocity = 1f;
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		print ("HI");
		while(true){}
		if(Input.GetButtonDown("Fire1")){
		
			//Requires tag called TBox to have been asssigned to a target of choice
			GameObject cube = GameObject.FindWithTag("TBox");
			
			aimAndFire (cube);
			
		}
	}
	
	public void aimAndFire(GameObject target){
			//Face the target
			//gameObject.transform.LookAt(new Vector3(target.transform.position.x,transform.position.y, target.transform.position.z) );
			
			Transform tempVec = transform;
			tempVec.LookAt(new Vector3(0, transform.position.y, 0));
			Vector3 targetVec = target.transform.position - tempVec.transform.position;
			Vector3 forwardVec = tempVec.forward;
			
			targetVec.y = forwardVec.y = 0;
			targetVec.Normalize();
			forwardVec.Normalize();
			float target_angle = Mathf.Atan2(targetVec.z, targetVec.x);
			float current_angle = Mathf.Atan2 (forwardVec.z, forwardVec.x);
			float baseAngle = target_angle - current_angle;
			//targetVec.Normalize();
			
			//float baseAngle = Mathf.Acos (Vector3.Dot (targetVec, forwardVec) / (targetVec.magnitude*forwardVec.magnitude));
			//Vector3 dotVec = Vector3.Dot(targetVec, transform.forward);
			//float baseAngle = Mathf.Acos ((targetVec.x*transform.forward.x + 0 + targetVec.z*transform.forward.z) / dotVec);
			
			baseAngle = baseAngle*Mathf.Rad2Deg;
			print(baseAngle);
			//baseAngle = Mathf.Acos(targetVec.transform.up);
			//transform.up = new Vector3(0,1,0);
			
			transform.Rotate(0, -baseAngle,0);
		
			//Initial angle
			float angle = 0;
			
			//While loop that ends when a bulletvelocity powerful enough to hit the target has been achieved.
			while(angle == 0){
				bulletVelocity++;
				angle = calcAngle(target);
			}
			
			
			//print(angle);
			//print("final v = " +  bulletVelocity);
			
			//Take aim....
			gameObject.transform.Rotate(angle,0,0);
			
			//Load...
			Rigidbody bulletclone = (Rigidbody)Instantiate(bullet, transform.position + transform.up, transform.rotation);
			
			//FIRE!!!
			bulletclone.velocity = transform.up*bulletVelocity;
			
			//Reset velocity so that the search for minimum velocity can restart
			bulletVelocity = 1f;
		
		
	}
	
	
	
	
	/**
	 * Method that calculates the angle that the cannon should have to hit target, according to the equation at 
	 * 
	 * http://en.wikipedia.org/wiki/Trajectory_of_a_projectile#Angle_required_to_hit_coordinate_.28x.2Cy.29
	 * 
	 * 
	 */
	float calcAngle(GameObject target){
	
		//Get the vector of the target and the cannon
		Vector3 targetVec = target.transform.position;
		Vector3 cannonVec = gameObject.transform.position;
		
		//Get the height difference, y
		float y = targetVec.y - cannonVec.y;
		//print ("y = " + y);
		
		//Set y to 0 so that the magnitude operation below only returns the length in the x plane, x
		targetVec.y = cannonVec.y = 0;
		
		//Get the length of x
		float x = (targetVec - cannonVec).magnitude;
		//print ("x = " + x);
		
		//print("v = " +bulletVelocity);
		
		//Gravity, negated because defined as such in unity.
		float g = -Physics.gravity.y;
		//print("g = " +g);
		
		//Inner square root in the ballistics equation
		float sqrt = (bulletVelocity*bulletVelocity*bulletVelocity*bulletVelocity) - (g * ((g * (x*x)) + (2 * y * (bulletVelocity*bulletVelocity))));
	
		//If sqrt is negative, the root below will be an imaginary number, which means that the target is out of range.
		if(sqrt < 0){
			return 0.0f;
		}
		
		//Squareroot of all evil.
		sqrt = Mathf.Sqrt(sqrt);
		
		//Maths. "90 - results" because that's sort of how the rotations of the cannon matrix works and because what else to spend
		//4+ hrs debugging...
		return 90 - (Mathf.Atan(((bulletVelocity*bulletVelocity) + sqrt) / (g*x)))*Mathf.Rad2Deg;
		
		
		
	}
	
	
	
	
}
 
