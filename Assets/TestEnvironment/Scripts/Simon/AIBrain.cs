using UnityEngine;
using System.Collections;

public class AIBrain : MonoBehaviour {
	
	enum GRID_DIRECTIONS{XUP, XDOWN, YUP, YDOWN, ZUP, ZDOWN};
	
	private int counter = 0;
	private float threatLevel = 0;
	private float opportunityLevel = 0;
	private Rigidbody bestAmmunition;
	private float bestAttackAngleTurn = 0;
	private float bestAttackAnglePitch = 0;
	private GameObject rayShooter;
	private GameObject heart;
	private float bulletVelocity = 1f;
	private IT2_CannonShoot cannonScript;
	private IT2_BuildScript buildScript;
	
	//Bool for waking the AI
	public bool isAwake = false;
	//private AIState CurrentState;
	// Use this for initialization
	IEnumerator Start () {
		yield return new WaitForSeconds(0.2f);
		//PLACEHOLDERS
		rayShooter = new GameObject("Rayshooter");
		heart = GetComponent<IT2_Player>().currentHeart;
		cannonScript = GetComponent<IT2_CannonShoot>();
		buildScript = GetComponent<IT2_BuildScript>();
	
	}
	
	// Update is called once per frame
	void Update () {
		Debug.Log("1");
		//StartCoroutine(Blocker ());
	
	}
	
	IEnumerator Blocker(){
		Debug.Log("2");
		while(true){
			yield return null;
			Debug.Log("3");
			counter++;
			if(counter == 10){
				counter = 0;
				yield break;
			}
		}
	}
	
	IEnumerator Manager(){
		StartCoroutine(BuildPhaseBhaviour());
		
		StartCoroutine(BattleBehaviour());
		yield break;
		
	}
	
	IEnumerator BuildPhaseBhaviour(){
		int xLowerLimit = 10;
		int xUpperLimit = 20;
		int zLowerLimit = 10;
		int zUpperLimit = 20;
		int height = 10;
		
		
		
		
		
		yield break;
	}
	
	IEnumerator BattleBehaviour(){
		while(true){
			yield return null;
		}
	}
	
	private void AnalyzeDefence(){
		rayShooter.transform.position = heart.transform.position;
		//TODO: Aim at opponents cannon
		rayShooter.transform.Rotate(20,0,0);
		RaycastHit rh;
		bool wasHit;
		//Foreach opponent cannon.
		for(int i = 0; i < 66; i++){
			wasHit = Physics.Raycast(rayShooter.transform.position, rayShooter.transform.forward,  out rh, Mathf.Infinity, 1);
			if(wasHit){
				//Was that our defence or something else? If something else increment threat meter.
				
			} else{
				//Increment threat meter.
			}
			
		}	
		
	}
	
	private void AnalyzeOpportunity(){
	}
	
	
	
	IEnumerator Shoot(GameObject target, Rigidbody ammunition){
		
		
		bulletVelocity = 1f;
		//Face the target
		
		Transform tempVec = transform;
		Vector3 targetVec = target.transform.position - tempVec.transform.position;
		Vector3 forwardVec = new Vector3(0,0,1);
		
		targetVec.y = forwardVec.y = 0;
		targetVec.Normalize();
		forwardVec.Normalize();
		float target_angle = Mathf.Atan2(targetVec.z, targetVec.x);
		float current_angle = Mathf.Atan2 (forwardVec.z, forwardVec.x);
		float baseAngle = target_angle - current_angle;
		
		baseAngle = baseAngle*Mathf.Rad2Deg;
		print(baseAngle);

		print("Called turn with angle " + baseAngle);

		cannonScript.turnToTarget(180 - baseAngle);
	
		//Initial angle
		float angle = 0;
		
		//While loop that ends when a bulletvelocity powerful enough to hit the target has been achieved.
		while(angle == 0){
			bulletVelocity++;
			angle = calcAngle(target);
		}
		
		print("Called pitch with " + angle);
		cannonScript.pitchToTarget(angle);
		
		yield return new WaitForSeconds(0.1f);
		while(cannonScript.isMoving){
			yield return null;
		}
			
		
		
		yield break;
	}
	
	IEnumerator Repair(){
		yield break;
	}
	
	IEnumerator Wait(){
		yield break;
		
	}
	
	void moveToGridCoords(int x, int y, int z){
		Vector3 thisPos = buildScript.getPosInGrid();
		buildScript.moveX((int)thisPos.x - x);
		buildScript.moveY ((int)thisPos.y - y);
		buildScript.moveZ ((int)thisPos.z - z);
	}
	
	void moveToGridCoords(Vector3 coords){
		Vector3 thisPos = buildScript.getPosInGrid();
		buildScript.moveX((int)thisPos.x - (int)coords.x);
		buildScript.moveY ((int)thisPos.y - (int)coords.y);
		buildScript.moveZ ((int)thisPos.z - (int)coords.z);
		
	}
	
	void buildWall(int length, int height, GRID_DIRECTIONS direction, Vector3 startPos){
		
		moveToGridCoords(startPos);
		
		if(direction == GRID_DIRECTIONS.XUP){
			for(int vertical = 0; vertical < height;vertical++){
				for( int horizontal = 0; horizontal < length;horizontal++){
					moveToGridCoords((int)startPos.x + horizontal, vertical, (int)startPos.z);
					buildScript.placeBlock();
				}
			}
		}
		if(direction == GRID_DIRECTIONS.XDOWN){
			for(int vertical = 0; vertical < height;vertical++){
				for( int horizontal = length; horizontal > 0;horizontal--){
					moveToGridCoords((int)startPos.x + horizontal, vertical, (int)startPos.z);
					buildScript.placeBlock();
				}
			}
		}
		if(direction == GRID_DIRECTIONS.ZUP){
			for(int vertical = 0; vertical < height;vertical++){
				for( int horizontal = 0; horizontal < length;horizontal++){
					moveToGridCoords((int)startPos.x, vertical, (int)startPos.z + horizontal);
					buildScript.placeBlock();
				}
			}
		}
		
		if(direction == GRID_DIRECTIONS.ZDOWN){
			for(int vertical = 0; vertical < height;vertical++){
				for( int horizontal = length; horizontal > 0;horizontal--){
					moveToGridCoords((int)startPos.x, vertical, (int)startPos.z + horizontal);
					buildScript.placeBlock();
				}
			}
		}
		
		
		
	}
	
	
	/**
	 * Method that calculates the angle that the cannon should have to hit target, according to the equation at 
	 * 
	 * http://en.wikipedia.org/wiki/Trajectory_of_a_projectile#Angle_required_to_hit_coordinate_.28x.2Cy.29
	 * 
	 * 
	 */
	float calcAngle(GameObject target){
	
		//Get the vector of the target and the cannon
		Vector3 targetVec = target.transform.position;
		Vector3 cannonVec = gameObject.transform.FindChild("Sphere001").position;
		
		//Get the height difference, y
		float y = targetVec.y - cannonVec.y;
		//print ("y = " + y);
		
		//Set y to 0 so that the magnitude operation below only returns the length in the x plane, x
		targetVec.y = cannonVec.y = 0;
		
		//Get the length of x
		float x = (targetVec - cannonVec).magnitude;
		//print ("x = " + x);
		
		//print("v = " +bulletVelocity);
		
		//Gravity, negated because defined as such in unity.
		float g = -Physics.gravity.y;
		//print("g = " +g);
		
		//Inner square root in the ballistics equation
		float sqrt = (bulletVelocity*bulletVelocity*bulletVelocity*bulletVelocity) - (g * ((g * (x*x)) + (2 * y * (bulletVelocity*bulletVelocity))));
	
		//If sqrt is negative, the root below will be an imaginary number, which means that the target is out of range.
		if(sqrt < 0){
			return 0.0f;
		}
		
		//Squareroot of all evil.
		sqrt = Mathf.Sqrt(sqrt);
		
		//Maths. "90 - results" because that's sort of how the rotations of the cannon matrix works and because what else to spend
		//4+ hrs debugging...
		return (Mathf.Atan(((bulletVelocity*bulletVelocity) + sqrt) / (g*x)))*Mathf.Rad2Deg;
		
		
		
	}
	
	
	
	
}
