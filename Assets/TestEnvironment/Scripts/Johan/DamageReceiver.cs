using UnityEngine;
using System.Collections;

public class DamageReceiver : MonoBehaviour {
	
	//The health of the object
	public float health = 25;
	
	public bool isExplosive = false;
	public float explosionPower = 10;
	public float explosionRadius = 5;
	

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
		
	}
	
	void OnCollisionEnter(Collision c){
		
		//Ignore small hits
	 	if (c.relativeVelocity.magnitude > 1.0) {
			
			float dmg;
			//if colliding with an object with a rigidbody (another object) multiply with the other object's mass
			if(c.gameObject.rigidbody != null){
				
				dmg = Mathf.Abs(Vector3.Dot(c.contacts[0].normal,c.relativeVelocity) * c.gameObject.rigidbody.mass);
				
			}
			//if colliding with a body without a rigidbody (e.g. the ground) don't multiply with the mass of the other object
			else{
				dmg = Vector3.Dot(c.contacts[0].normal,c.relativeVelocity);
			}
			
   	 		health -= dmg;
			print("Damage dealt to " + gameObject.name + ": " + dmg + ". Collission between [" + gameObject.name + "] and [" + c.gameObject.name+ "].");
    	}
		
	 	if (health <= 0) {
			
			//Do explosion
			if(isExplosive){
				
				Collider[] colliders = Physics.OverlapSphere(transform.position, explosionRadius);
				foreach(Collider col in colliders){
					
					if(col.rigidbody){
						col.rigidbody.AddExplosionForce(explosionPower, transform.position, explosionPower);	
					}
					
				}
				
			}
			
			//Remove destroyed object
   	 		Destroy(gameObject);
		}
		
	}
}
