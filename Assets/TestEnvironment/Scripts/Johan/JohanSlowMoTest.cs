using UnityEngine;
using System.Collections;

public class JohanSlowMoTest : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
		if(Input.GetKey(KeyCode.UpArrow) && Time.timeScale <= 1.0f){
			Time.timeScale += 0.01f;
		}
		else if(Input.GetKey(KeyCode.DownArrow) && Time.timeScale >= 0.0f){
			Time.timeScale -= 0.01f;
		}
	}
}
