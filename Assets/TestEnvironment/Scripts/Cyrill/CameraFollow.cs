using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour {
	
	
	public Transform targetOther;
	public Transform camTarget;
	public Transform lookAtTarget;
	
	public float maxSpeed = 10.0f;
	public float smoothLag = 0.2f;
	public float heightOffset = 0.1f;
	public float xOffset = 0.1f;
	public float zOffset = 0.1f;
	public float diff = 0.1f;
	
	public float snapLag = 0.3f;
	public bool isMoving;
	
	private Vector3 velocity = Vector3.zero;
	
	// Use this for initialization
	
	void Start () {
		
		isMoving = false;
	
	} 

 
	void Update()
	{
	     //transform.position = (target.transform.position);
		
		if (isMoving)
		{
			ApplyPositionTransistion();
			//ApplyCanonTrans ();
		}
		
	}
	
	
	
	private void ApplyPositionTransistion()
	{
		//Position
		Vector3 position = transform.position;
		Vector3 target = camTarget.position;
		Vector3 target_pos = target;
		
		
		//Camera.main.GetComponent<CameraFollow>().camTarget = GetComponent<IT2_GameManager>().getLocalPlayer().currentCannon.transform.Find("CameraPoint");
			
		//target.x += xOffset;
		//target.z += zOffset;
		//target.y += heightOffset;
		
		Vector3 newPosition;
		newPosition.x = Mathf.SmoothDamp (position.x,target.x,ref velocity.x,smoothLag,maxSpeed );
		newPosition.y = Mathf.SmoothDamp (position.y,target.y,ref velocity.y,smoothLag,maxSpeed );
		newPosition.z = Mathf.SmoothDamp (position.z,target.z,ref velocity.z,smoothLag,maxSpeed );
		
		//Mathf.SmoothDampAngle
		if (WithinOffset ())
		{
			isMoving=false;
			Debug.Log ("Camera Transition stopped");
		}
		
		transform.position = newPosition;
		
		//transform.LookAt (camTarget.Find ("LookAtTarget") );
		//print (camTarget.Find ("LookAtTarget").transform.position);
		transform.LookAt (lookAtTarget );
		transform.LookAt (camTarget.transform.Find ("LookAtPoint") );
		
		
	}
	
	public void setNewTarget(Transform tar)
	{
		camTarget = tar;
		MoveToTarget();
	}
	
	public void MoveToTarget()
	{
		isMoving= true;
	}
	
	public void setDistanceHigh()
	{
		zOffset = 50f;
		xOffset = 50f;
	}
	
	public void setDistanceNear()
	{
		zOffset = 50f;
		xOffset = 50f;
	}
	
	bool WithinOffset()
	{
		Vector3 target = camTarget.position ;
		Vector3 pos = transform.position;
		
		if((pos.x-target.x)<=xOffset+diff) Debug.Log ("cond1");
		if((pos.y-target.y)<=heightOffset+diff) Debug.Log ("cond2");
		if((pos.z-target.z)<=zOffset+diff)Debug.Log ("cond3");
		
		if (((pos.x-target.x)<=xOffset+diff) && ((pos.y-target.y)<=heightOffset+diff) && ((pos.z-target.z)<=zOffset+diff ))
		{
			Debug.Log ("x=" + (pos.x-target.x) + "z=" + (pos.z-target.z) + "y=" + (pos.y-target.y));
			
			return true;
		} else {
			return false;
		}
	}
	
}