using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Net.NetworkInformation;
using System.Diagnostics;

// TODO cleanup

public class SeamlessNetworkPlay : MonoBehaviour
{
    private enum enuState { NotActive, Searching, IsServer, IsClient };
    enuState currentState = enuState.NotActive;
    private struct ReceivedMessage { public float fTime; public string strIP; public bool bIsReady;}
	private struct ServerBeacon { public float lastPing; public IPAddress local_IP; public UdpClient beacon;}
	List<ServerBeacon> beacons;
    private struct ReceiveOperationInfo { public UdpClient objUDPClient; public IPEndPoint objIPEndPoint;}
	
	int udp_port = 12893;			// port through which server pings about it's existance
	int server_port = 6983;			// port on which server listens for new connections
	
	string target_server_ip;
	int target_server_port;
	
	IPEndPoint broadcastEndpoint;
	IPEndPoint listenEndpoint;
	
	UdpClient clientUDP;
	bool receiving = false;
	bool connecting = false;
	
	Stopwatch sw;
	int scanTime = 100;
	int beaconInterval = 1;
	
    void Start () {
		sw = new Stopwatch();
		broadcastEndpoint = new IPEndPoint(IPAddress.Broadcast, udp_port);
		listenEndpoint = new IPEndPoint(IPAddress.Any, udp_port);
		clientUDP = new UdpClient(udp_port);
		// finding all network cards which can be used for connections
		NetworkInterface[] nics = NetworkInterface.GetAllNetworkInterfaces();
		if (nics == null || nics.Length < 1) {
			UnityEngine.Debug.Log("  No network interfaces found.");
			return;
		}
		beacons = new List<ServerBeacon>();
		foreach (NetworkInterface adapter in nics) {
			NetworkInterfaceType type = adapter.NetworkInterfaceType;
			OperationalStatus status = adapter.OperationalStatus;
			bool ipv4support = adapter.Supports(NetworkInterfaceComponent.IPv4);
			if ((type == NetworkInterfaceType.Wireless80211 || type == NetworkInterfaceType.Ethernet) && status == OperationalStatus.Up && ipv4support) {
				UnicastIPAddressInformationCollection addresses = adapter.GetIPProperties().UnicastAddresses;
				foreach (UnicastIPAddressInformation ipadd in addresses) {
					if (ipadd.Address.AddressFamily == AddressFamily.InterNetwork) {
						ServerBeacon temp = new ServerBeacon();
						temp.lastPing = Time.time;
						temp.local_IP = ipadd.Address;
						UnityEngine.Debug.Log(temp.local_IP);
						temp.beacon = new UdpClient(new IPEndPoint(temp.local_IP, 0));
						temp.beacon.EnableBroadcast = true;
						beacons.Add(temp);
					}
				}
			}
		}
		currentState = enuState.Searching;
		
    }
    // Update is called once per frame
    void Update() {
		switch (currentState) {
		case enuState.IsServer:
			// send beacon pings via all network cards
			foreach (ServerBeacon serverBeacon in beacons) {				
				byte[] messageToSend = Encoding.UTF8.GetBytes("simengine/"+serverBeacon.local_IP+"/"+server_port);
				int iSentBytes = serverBeacon.beacon.Send(messageToSend, messageToSend.Length, broadcastEndpoint);
			}
			Thread.Sleep(beaconInterval);
			break;
		case enuState.Searching:
			// listen for pings from servers
			if (!receiving) {
				receiving = true;
				sw.Start();
				BeginAsyncReceive();
			} else {
				if (sw.Elapsed >= TimeSpan.FromMilliseconds(scanTime)) {
					UnityEngine.Debug.Log("Timeout, time to create server");
					sw.Stop();
					clientUDP.Close();
					currentState = enuState.IsServer;
					Network.InitializeServer(32, server_port, false);
					Thread.Sleep(10);
				}
			}
			break;
		case enuState.IsClient:
			if (!connecting) {
				connecting = true;
				Network.Connect(target_server_ip, target_server_port);
			}
			break;
		}
    }
    // Method to start an Async receive procedure
    private void BeginAsyncReceive() {
		UnityEngine.Debug.Log("Trying to scan for packets");
        ReceiveOperationInfo objInfo = new ReceiveOperationInfo();
        objInfo.objUDPClient = clientUDP;
        objInfo.objIPEndPoint = listenEndpoint;
        clientUDP.BeginReceive(new AsyncCallback(EndAsyncReceive), objInfo);
    }
    // Callback method from the UDPClient, when the async receive procedure received a message
    private void EndAsyncReceive(IAsyncResult objResult) {
        byte[] byteData = clientUDP.EndReceive(objResult, ref listenEndpoint);
        if (byteData.Length > 0)
        {
            string text = Encoding.UTF8.GetString(byteData);
			string[] split_data = text.Split('/');
			UnityEngine.Debug.Log("got message: "+text);
			if (split_data.Length >= 3) {
				string s_code = split_data[0];
				string s_ip = split_data[1];
				string s_port = split_data[2];
				if (s_code.Equals("simengine", StringComparison.OrdinalIgnoreCase)) {
					UnityEngine.Debug.Log("Received server info, connecting..."+s_ip+":"+s_port);
					currentState = enuState.IsClient;
					target_server_ip = s_ip;
					target_server_port = Convert.ToInt32(s_port);
					return;
				}
			}
        }
		BeginAsyncReceive();
    }
	
	void OnDisable() {
		foreach (ServerBeacon serverBeacon in beacons) {
			serverBeacon.beacon.Close();
		}
		clientUDP.Close();
		Network.Disconnect();
	}

}