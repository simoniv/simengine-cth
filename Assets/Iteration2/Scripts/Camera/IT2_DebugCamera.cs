using UnityEngine;
using System.Collections;

public class IT2_DebugCamera : MonoBehaviour {

public float movementSpeed = 0.7f;
	private float rotationSpeedX = 1.0f;
	private float rotationSpeedY = 10.0f;
	
	private float angleX;
	private float angleY;
	
	// Use this for initialization
	void Start () {
	
		angleX=transform.eulerAngles.x;
		angleY=transform.eulerAngles.y;
		
	}
	
	// Update is called once per frame
	void Update () {
	
		if ((IT2_InputSelector.activeInput == IT2_InputSelector.activeControl.debug))
		{
			if( Input.GetMouseButton( 1) )
			{			
				angleX = - Input.GetAxis("Mouse Y") * rotationSpeedX;
	        	angleY = Input.GetAxis("Mouse X") * rotationSpeedY;
				transform.Rotate (angleX,0,0,Space.Self);
				transform.Rotate (0,angleY,0,Space.World);
							
				//Get the movement Buttons: wasd or arrows
				Vector3 movement = (Input.GetAxis("Horizontal") * -Vector3.left * movementSpeed)
					+ (Input.GetAxis("Vertical") * Vector3.forward *movementSpeed);
				
				transform.Translate(movement);
				
				//Use q and e for up and down movement moves in
				if(Input.GetKey (KeyCode.Q))
				{ 
					movement = - Vector3.up * movementSpeed;
					transform.Translate (movement,Space.World);
					
				} else if(Input.GetKey (KeyCode.E))
				{
					movement =  Vector3.up * movementSpeed;
					transform.Translate (movement,Space.World);
				} 		
			
			}
		}
	}
}
