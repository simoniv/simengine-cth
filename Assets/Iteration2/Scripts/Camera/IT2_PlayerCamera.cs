using UnityEngine;
using System.Collections;

public class IT2_PlayerCamera : MonoBehaviour {

	public Transform camTarget; //CameraTarget must have a child called "LookAtPoint" which indicates the default direction of the camera
	
	//Parameters for the transitions
	public float maxSpeed = 10.0f;
	public float smoothLag = 0.2f;
	public bool smoothRotation = true;
	public float damping = 6.0f;
	
	public float diff = 0.1f;
	public float diffDeg = 1.0f;
	
	public bool isMoving;
	public bool isRotating;
	
	private Vector3 velocity = Vector3.zero;
	private float rotationSpeedX = 1.0f;
	private float rotationSpeedY = 10.0f;
	
	// Use this for initialization	
	void Start () 
	{		
		isMoving = false;
		isRotating = false;	
	} 

 
	void Update()
	{   
		
		if (isMoving)
		{
			ApplyPositionTransistion();			
		} 		
		
		if (isRotating)
		{
			LookAtPoint();			
		}
		
	}
	
	//Read 
	private void LookAround()
	{
		if( Input.GetMouseButton( 1))
		{
			float angleX, angleY;
			angleX = - Input.GetAxis("Mouse Y") * rotationSpeedX;
        	angleY = Input.GetAxis("Mouse X") * rotationSpeedY;
			transform.Rotate (angleX,0,0,Space.Self);
			transform.Rotate (0,angleY,0,Space.World);
		}
	}
	
	private void ApplyPositionTransistion()
	{
		//Position
		Vector3 position = transform.position;
		//If camera target does not exist go to 0,0,0 instead
		Vector3 target = camTarget != null ? camTarget.position : new Vector3();			
		Vector3 newPosition;	
		
		newPosition.x = Mathf.SmoothDamp (position.x,target.x,ref velocity.x,smoothLag,maxSpeed );
		newPosition.y = Mathf.SmoothDamp (position.y,target.y,ref velocity.y,smoothLag,maxSpeed );
		newPosition.z = Mathf.SmoothDamp (position.z,target.z,ref velocity.z,smoothLag,maxSpeed );		
		
		/* Currently not used
		if (WithinOffset ())
		{			
			isMoving=false;
			//Debug.Log ("Camera Transition stopped");
		}		
		*/
		transform.position = newPosition;
	}
	
	public void LookAtPoint()
	{		
		Transform target = camTarget.transform.Find("LookAtPoint");		
		
		if (smoothRotation)
		{
			// Look at and dampen the rotation
			Quaternion rotation = Quaternion.LookRotation(target.position - transform.position);
			transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * damping);
		}
		else
		{
			// Just lookat
		    transform.LookAt(target);
		}
		/* Currently not used
		if (RotationWithinOffset())
		{
			isRotating = false;
		}
		*/
	}
	
	//Set new Target and start transition
	public void setNewTarget(Transform tar)
	{
		camTarget = tar;
		MoveToTarget();
		RotateToLookAtPoint();
	}
	
	public void MoveToTarget()
	{
		isMoving= true;
	}
	
	public void RotateToLookAtPoint()
	{
		isRotating = true;		
	}
		
	private bool WithinOffset()
	{
		Vector3 target = camTarget != null ? camTarget.position : new Vector3();
		Vector3 pos = transform.position;
		
		/*
		if((pos.x-target.x)<=diff) Debug.Log ("cond1");
		if((pos.y-target.y)<=diff) Debug.Log ("cond2");
		if((pos.z-target.z)<=diff)Debug.Log ("cond3");
		*/
		if ((Mathf.Abs(pos.x-target.x)<=diff) && (Mathf.Abs(pos.y-target.y)<=diff) && (Mathf.Abs(pos.z-target.z)<=diff ))
		{
			//Debug.Log ("x=" + (pos.x-target.x) + "z=" + (pos.z-target.z) + "y=" + (pos.y-target.y));
			return true;
		} else {
			return false;
		}
	}
	
	private bool RotationWithinOffset()
	{
		Transform target = camTarget != null ? camTarget.transform.Find ("LookAtPoint") : transform;
		Quaternion rotation = Quaternion.LookRotation(target.position - transform.position);
		
		//print ("x" + target.eulerAngles.x + "x" + transform.eulerAngles.x);
		
		if(Mathf.Abs(target.eulerAngles.x-transform.eulerAngles.x)<=diffDeg && 
			Mathf.Abs(target.eulerAngles.y-transform.eulerAngles.y)<=diffDeg &&
			Mathf.Abs(target.eulerAngles.z-transform.eulerAngles.z)<=diffDeg)
		{
			print ("yehaaa, angles are within borders"); // doesn work at all right now
			return true;
		} else 
		{
			print ("Function RotationWithinOffset not implemented!");
			return false;
		}		
	}		
}
