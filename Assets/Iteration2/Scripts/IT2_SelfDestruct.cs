using UnityEngine;
using System.Collections;

public class IT2_SelfDestruct : MonoBehaviour {
	
	//Timed self-destruct
	private float destructTime;
	private float startTime;
	private bool setToSelfDestruct = false;
	
	//Bounds that contain the object
	private Bounds bounds;
	private bool boundsDestruct = false;
	
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		
		//If timed selfdestruct is on and time has run out
		if(setToSelfDestruct && (Time.realtimeSinceStartup-startTime) > destructTime){
			print ("Object selfdestructing after time has run out");
			Destroy(gameObject);
		}
		
		//If outside of bounds
		if(!bounds.Contains(transform.position) && boundsDestruct){
			print ("Object selfdestructing after going out of bounds");
			Destroy(gameObject);	
		}
	}
	
	/*
	 * Despawn object if outside of the bounds
	 */
	public void setDespawnBounds(Bounds b){
		bounds = b;
		boundsDestruct = true;
	}
	
	public void timerDestruct(int s){
		//Save time in s
		startTime = Time.realtimeSinceStartup;
		destructTime = s;
		setToSelfDestruct = true;
	}
}

