using UnityEngine;
using System.Collections;

public class IT2_DamageReceiver : MonoBehaviour {
	
	//The health of the object
	public float health = 25;
	
	public bool isExplosive = false;
	public bool canTakeDamage = false;
	public float explosionPower = 10;
	public float explosionRadius = 5;
	
	//Workaround for self-destructing objects
	public GameObject audiokeeper;
	
	public AudioClip impactSound;
	
	public GameObject explosionSystem;
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
		
	}
	
	void OnCollisionEnter(Collision c){
		
		if(canTakeDamage){
			//Ignore small hits
		 	if (c.relativeVelocity.magnitude > 1.0) {
				
				float dmg;
				//if colliding with an object with a rigidbody (another object) multiply with the other object's mass
				if(c.gameObject.rigidbody != null){
					dmg = Mathf.Abs(Vector3.Dot(c.contacts[0].normal,c.relativeVelocity) * c.gameObject.rigidbody.mass);
				}
				//if colliding with a body without a rigidbody (e.g. the ground) don't multiply with the mass of the other object
				else{
					dmg = Vector3.Dot(c.contacts[0].normal,c.relativeVelocity);
				}
				
	   	 		health -= dmg;
				print("Damage dealt to " + gameObject.name + ": " + dmg + ". Collission between [" + gameObject.name + "] and [" + c.gameObject.name+ "].");
	    	}
		
		 	if (health <= 0) {
				
				//Do explosion
				if(isExplosive){
					
					((GameObject)Instantiate(audiokeeper, transform.position, transform.rotation)).audio.Play();
					createExplosion();
					
					Collider[] colliders = Physics.OverlapSphere(transform.position, explosionRadius);
					foreach(Collider col in colliders){
						
						if(col.rigidbody){
							col.rigidbody.AddExplosionForce(explosionPower, transform.position, explosionPower);	
						}
						
					}
					
				}
				
				//TODO Make sure it's deleted from the array as well
	   	 		Network.Destroy(gameObject.networkView.viewID);
			}
		} 
		if(c.relativeVelocity.magnitude > 1.0){
			audio.PlayOneShot(impactSound);
		}
		
		if(gameObject.name == "Cannonball(Clone)"){
			//GetComponent("Ludvig Particle System");
			Destroy(gameObject.GetComponent("Ludvig Particle System"));
			gameObject.networkView.RPC("destroyParticles", RPCMode.AllBuffered);
		}
		
		
	}
	
	void createExplosion(){
		Network.Instantiate(explosionSystem, transform.position, transform.rotation, 0);
	}
}
