using UnityEngine;
using System.Collections;

public class MenuCameraControl : MonoBehaviour {
	
	// Use this for initialization
	void Start () {
		/*Fit entire menu plane on screen by moving the camera back to accomondate for screen aspect ratios taller than 16:9, which the menu is designed for */
		//Calculate the amount to move the camera back, 16:9 is base AR, resulting in no scaling. 
		//"ar" variable stores how much the aspect ratio differs from 16:9
		float ar = (1- ( ((float)Screen.width / (float)Screen.height) / (float)1.6 ));
		//if aspect ratio deviation is over a reasonable limit, move camera back
		if(0.1f -ar < 0)
				ar = -15*ar; //move camera back
		//Move camera (scale menu on screen)
		gameObject.transform.Translate(0.0f,0.0f,ar);
		
	}
	// Update is called once per frame
	void Update () {

	}

}
