using UnityEngine;
using System.Collections;

public class IT2_AIBrain : MonoBehaviour {
	
	//Enums!
	public enum GRID_DIRECTIONS{XUP, XDOWN, YUP, YDOWN, ZUP, ZDOWN};
	public enum DIFFICULTY{VERY_EASY = 0, EASY = 1, NORMAL = 2, HARD = 3, VERY_HARD = 4};
	
	//Fuzzy logic parameters
	private const float threatCutoffLevel = 0.6f;
	private float opportunityLevel = 0;
	
	//Not in use yet
	private Rigidbody bestAmmunition;
	private float bestAttackAngleTurn = 0;
	private float bestAttackAnglePitch = 0;
	
	//Ray shooter object for defence analysis
	private GameObject rayShooter;
	
	//Difficulty (duh!)
	private DIFFICULTY diff = DIFFICULTY.NORMAL;
	
	//Arrays for repair analysis
	private int[,,] desiredBuild;
	private int[,,] currentBuild;
	private ArrayList displacedBlocks;
	
	//speed variables
	private float buildSpeedDelay = 0.1f;
	private float standardShootDelay = 3.0f;
	private float standardShootDelayDeviation = 2.0f;
	
	//Friend objects
	private IT2_Player player;
	private GameObject heart;
	private GameObject cannon;
	private IT2_CannonShoot cannonScript;
	private IT2_BuildScript buildScript;
	
	//Start bullet velocity
	private float bulletVelocity = 1f;

	//Foe objects
	private IT2_Player opponentPlayer;
	private GameObject opponentHeart;
	private GameObject opponentCannon;
	
	//Bool for waking the AI
	public bool awake = false;
	
	//Ugly hack
	private bool firstRun = true;
	
	private bool hasBuilt = false;
	
	
	//private AIState CurrentState;
	
	
	// Use this for initialization
	IEnumerator Start () {

		rayShooter = new GameObject("Rayshooter");
		displacedBlocks = new ArrayList();

		yield break;
	
	}
	
	// Update is called once per frame
	void Update () {
		if(firstRun && awake){

			firstRun = false;
			
			StartCoroutine(Manager ());	
		}
	
	}
	
	
	/**
	 * Sequentially start the different parts.
	 * 
	 */
	IEnumerator Manager(){

		yield return StartCoroutine(DataFetch ());
		
		if(!hasBuilt){
			hasBuilt = true;
			yield return new WaitForSeconds(15.0f);
			yield return StartCoroutine(BuildPhaseBhaviour());

		}
		
		yield return StartCoroutine(BattleBehaviour());
		yield break;
		
	}
	
	
	/**
	 * Fetches initial data about the screen.
	 * TODO: Could be a lot safer with try-catches...
	 * 
	 */
	IEnumerator DataFetch(){
		
		//Get own  playerobject
		player = GetComponent<IT2_Player>();
		
		//Get own heart
		heart = player.currentHeart;
		while(!heart.name.Contains("(Clone)")){
			heart = GetComponent<IT2_Player>().currentHeart;
			yield return null;
		}

		//Get own cannon + script
		cannon = player.currentCannon;
		cannonScript = cannon.GetComponent<IT2_CannonShoot>();
		print(cannonScript.ToString());
		while(!cannon.name.Contains("(Clone)")){
			cannon = player.currentCannon;
			cannonScript = cannon.GetComponent<IT2_CannonShoot>();

			yield return null;
		}

		//Get own buildscript
		buildScript = player.currentBuildingArea.GetComponent<IT2_BuildScript>();
		while(buildScript == null){
			buildScript = player.currentBuildingArea.GetComponent<IT2_BuildScript>();
			yield return null;
		}
	
		//Initialize the array used for repair
		if(!hasBuilt){
			int width = buildScript.width;
			int height = buildScript.height;
			desiredBuild = new int[width,height,width];
			currentBuild = new int[width,height,width];
		}
		
		
		//Get opponent data
		ArrayList playerList = IT2_GameManager.playerScripts;
		foreach(IT2_Player potentialScript in playerList){

			if(!(potentialScript == player)){
				opponentPlayer = potentialScript;
				opponentHeart = opponentPlayer.currentHeart;
				opponentCannon = opponentPlayer.currentCannon;
				break;
			}
		}

		yield break;
	}
	
	
	/**
	 * How the AI works during the build phase
	 * 
	 */
	IEnumerator BuildPhaseBhaviour(){
		
		float midpoint = buildScript.width / 2;
		
		//Make square structure
		yield return StartCoroutine(buildWall(6, 3, GRID_DIRECTIONS.XUP, new Vector3(midpoint - 10.0f,0.0f,midpoint - 10.0f)));
		yield return StartCoroutine(buildWall(6, 3, GRID_DIRECTIONS.XDOWN, new Vector3(midpoint + 10.0f,0.0f,midpoint + 10.0f)));
		yield return StartCoroutine(buildWall(4, 3, GRID_DIRECTIONS.ZUP, new Vector3(midpoint - 10.0f,0.0f,midpoint - 6.0f)));
		yield return StartCoroutine(buildWall(4, 3, GRID_DIRECTIONS.ZUP, new Vector3(midpoint + 10.0f,0.0f,midpoint - 6.0f)));
		scanArea ();
		desiredBuild = new int[buildScript.width,buildScript.height,buildScript.width];
		desiredBuild = (int[,,])currentBuild.Clone();
		
		yield break;
	}
	/**
	 * How the AI works during battle
	 * 
	 */
	IEnumerator BattleBehaviour(){
		float threatLevel = 0.0f;
		while(true){
			
			yield return new WaitForSeconds(standardShootDelay + Random.Range (0.0f, standardShootDelayDeviation));
			if((heart != null) && (opponentHeart != null)){
				if(cannonScript.isReady()){
					yield return StartCoroutine(Shoot(opponentHeart));
				}
			
				threatLevel = AnalyzeDefence();
				if(threatLevel >= threatCutoffLevel){
					yield return StartCoroutine(Repair ());
				}
			}

		}
	}
	

	/**
	 * Analyzes how well defended the own heart is.
	 * 
	 */
	private float AnalyzeDefence(){
		
		RaycastHit rh;
		bool wasHit;
		float threatLevel = 0.0f;
		
		//Place the rayshooter at the heart and aim at opponents cannon
		rayShooter.transform.position = heart.transform.position;
		rayShooter.transform.LookAt(opponentCannon.transform);
		rayShooter.transform.Rotate(-10,0,0);
		
		//Foreach relevant angle
		for(int i = 0; i < 76; i++){
			
			//Cast rays to see what's between the heart and the opponent cannon
			wasHit = Physics.Raycast(rayShooter.transform.position, rayShooter.transform.forward,  out rh, Mathf.Infinity, 1);
			//Debug.DrawRay(rayShooter.transform.position, rayShooter.transform.forward, Color.white, 10, true);
			
			//Check what, if anything, that was between the heart and the opponent cannon
			//If whatever it was was a part of the own wall this specific angle is covered by this block
			//Otherwise something else was hit, like the opponents cannon, which will not protect the heart
			if(wasHit){
				foreach(GameObject brick in player.blocksPlaced){
					if(!brick.collider == rh.collider){
						threatLevel += 1.0f / 76.0f;
					}
				}
			} else{
				threatLevel += 1.0f / 76.0f;
			}
			
			rayShooter.transform.Rotate (-1,0,0);
			
		}
		return threatLevel;
		
	}
	
	/**
	 * Analyzes how well defended the opponent is.
	 * 
	 */
	private void AnalyzeOpportunity(){
	}
	
	/**
	 * Populates the CurrentBuild array with information on where blocks are.
	 */
	private void scanArea(){
		
		//Clear the array
		currentBuild = new int[buildScript.width, buildScript.height, buildScript.width];
		
		displacedBlocks.Clear ();
		
		//Loop through all blocks belonging to the AI
		foreach(GameObject brick in player.blocksPlaced){
			
			//Extra safety measure
			if(brick == null){
				continue;	
			}
			
			
			Vector3 currPos = brick.transform.position;
			
			currPos = buildScript.getRealCoordsToGrid(currPos);;
			
			//Due to inexactness of unity.
			if((currPos.y < 0) && (currPos.y > -0.1)){
				currPos.y = Mathf.Abs(currPos.y);
			}
			
			//Is margins tolerable?
			if((((currPos.x%1)<0.45)||((currPos.x%1)>0.55))
				&&
				(((currPos.y%1)<0.45)||((currPos.y%1)>0.55))
				&&
				(((currPos.z%1)<0.45)||((currPos.z%1)>0.55))
				){ 
				
				//Round to nearest array position
				currPos.x = Mathf.Round(currPos.x);
				currPos.y = Mathf.Round(currPos.y);
				currPos.z = Mathf.Round(currPos.z);
				
				//Is inside index?
				if(currPos.x<buildScript.width
					&&
					currPos.x>=0
					&&
					currPos.y<buildScript.height
					&&
					currPos.y>=0
					&&
					currPos.z<buildScript.width
					&&
					currPos.z>=0
					){
					
					currentBuild[(int)currPos.x, (int)currPos.y, (int)currPos.z] = 1;
				
				
				}else{
					displacedBlocks.Add(brick);
				}
				
				
			}else{
					displacedBlocks.Add(brick);
			}
			
		}
		
	}

	/**
	 * Tries to find somewhere to repair by comparing currentBuild with desiredBuild
	 */
	IEnumerator Repair(){
		
		//What's our current status?
		scanArea ();
		
		yield return null; //to not hold up CPU too much
		
		GameObject tempobj;
		
		//Loop through the grids
		for(int x = 0; x < buildScript.width; x++){
			yield return null;  //to not hold up the CPU too much...
			for(int z = 0; z < buildScript.width; z++){
				for(int y = 0; y < buildScript.height; y++){
					
					
					//If there should be a block here...
					if(desiredBuild[x,y,z] == 1){
						
						
						//... and there wasn't any....
						if(currentBuild[x,y,z] != 1){
							
							
							//... nor in any of the closest spots in z or x directions...
							if((currentBuild[Mathf.Max(x+1, buildScript.width-1),y,z] != 1) 
								&&
								(currentBuild[Mathf.Max(x-1, 0),y,z] != 1) 
								&&
								(currentBuild[x,y,Mathf.Max(z+1, buildScript.width-1)] != 1) 
								&&
								(currentBuild[x,y,Mathf.Max(z-1, 0)] != 1)
								){
							
								//... then place a block
								moveToGridCoords(x, y, z);
								yield return new WaitForSeconds(buildSpeedDelay);
								tempobj = buildScript.placeBlock();
								if(tempobj != null){
									player.blocksPlaced.Add(tempobj);
									yield break;
								}
							}
							
							
						} else if(y == 0){
							break;  //Speed up thing.
						}
						
					}
					
					
				}
			
			}
			
		}
		
		yield break;
	}
	
	
	/**
	 * Moves the builder marker to a set (x,y,z) (Grid coordinates, not real world) 
	 * 
	 */
	void moveToGridCoords(int x, int y, int z){
		
		Vector3 thisPos = buildScript.getPosInGrid();

		buildScript.moveX (x - (int)thisPos.x);
		buildScript.moveY (y - (int)thisPos.y);
		buildScript.moveZ (z - (int)thisPos.z);
		
		thisPos = buildScript.getPosInGrid();

	}
	
	/**
	 * Moves the builder marker to a set Vector3 (Grid coordinates, not real world) 
	 * 
	 */
	void moveToGridCoords(Vector3 coords){
		
		Vector3 thisPos = buildScript.getPosInGrid();

		buildScript.moveX ((int)coords.x - (int)thisPos.x);
		buildScript.moveY ((int)coords.y - (int)thisPos.y);
		buildScript.moveZ ((int)coords.z - (int)thisPos.z);
		
		thisPos = buildScript.getPosInGrid();
		
	}
	
	
	/**
	 * Builds a wall with the length, height and direction from a given start
	 * point (in the build-area grid, not real-world coordinates)
	 * 
	 */
	IEnumerator buildWall(int length, int height, GRID_DIRECTIONS direction, Vector3 startPos){

		moveToGridCoords(startPos);
		GameObject tempObj;
		
		if(direction == GRID_DIRECTIONS.XUP){
			for(int vertical = 0; vertical < height;vertical++){
				for( int horizontal = 0; horizontal < length;horizontal++){
					moveToGridCoords((int)startPos.x + horizontal*4, vertical*4, (int)startPos.z);
					yield return new WaitForSeconds(buildSpeedDelay);
					
					tempObj = buildScript.placeBlock();
					if(tempObj != null){
						player.blocksPlaced.Add(tempObj);
					}
					

				}
			}
		}
		if(direction == GRID_DIRECTIONS.XDOWN){
			for(int vertical = 0; vertical < height;vertical++){
				for( int horizontal = 0; horizontal < length;horizontal++){
					moveToGridCoords((int)startPos.x - horizontal*4, vertical*4, (int)startPos.z);
					yield return new WaitForSeconds(buildSpeedDelay);
					tempObj = buildScript.placeBlock();
					if(tempObj != null){
						player.blocksPlaced.Add(tempObj);
					}
				}
			}
		}
		if(direction == GRID_DIRECTIONS.ZUP){
			for(int vertical = 0; vertical < height;vertical++){
				for( int horizontal = 0; horizontal < length;horizontal++){
					moveToGridCoords((int)startPos.x, vertical*4, (int)startPos.z + horizontal*4);
					yield return new WaitForSeconds(buildSpeedDelay);
					tempObj = buildScript.placeBlock();
					if(tempObj != null){
						player.blocksPlaced.Add(tempObj);
					}
				}
			}
		}
		
		if(direction == GRID_DIRECTIONS.ZDOWN){
			for(int vertical = 0; vertical < height;vertical++){
				for( int horizontal = 0; horizontal < length;horizontal++){
					moveToGridCoords((int)startPos.x, vertical*4, (int)startPos.z - horizontal*4);
					yield return new WaitForSeconds(buildSpeedDelay);
					tempObj = buildScript.placeBlock();
					if(tempObj != null){
						player.blocksPlaced.Add(tempObj);
					}
				}
			}
		}
		
		
		
	}
	
	
	/**
	 * Calculates and shoots a Rigidbody at a target GameObject 
	 * 
	 */
	IEnumerator Shoot(GameObject target){
		
		bulletVelocity = 1f;
		//Face the target
		
		Transform tempVec = cannon.transform;
		Vector3 targetVec = target.transform.position - tempVec.transform.position;
		Vector3 forwardVec = new Vector3(0,0,1);
		
		targetVec.y = forwardVec.y = 0;
		targetVec.Normalize();
		forwardVec.Normalize();
		float target_angle = Mathf.Atan2(targetVec.z, targetVec.x);
		float current_angle = Mathf.Atan2 (forwardVec.z, forwardVec.x);
		float baseAngle = target_angle - current_angle;
		
		baseAngle = baseAngle*Mathf.Rad2Deg;

		cannonScript.turnToTarget((180 - baseAngle)*getDifficultyModifier(diff));
	
		//Initial angle
		float angle = 0;
		
		//While loop that ends when a bulletvelocity powerful enough to hit the target has been achieved.
		while(angle == 0){
			bulletVelocity++;
			angle = calcAngle(target);
		}
		
		cannonScript.pitchToTarget(angle*getDifficultyModifier(diff));
		
		//give cannon chance to start moving
		yield return new WaitForSeconds(0.1f);
		
		
		while(cannonScript.isMoving){
			yield return null;
		}
		cannonScript.shoot(bulletVelocity);
		
		
		yield break;
	}
	
	/**
	 * Method that calculates the angle that the cannon should have to hit target, according to the equation at 
	 * 
	 * http://en.wikipedia.org/wiki/Trajectory_of_a_projectile#Angle_required_to_hit_coordinate_.28x.2Cy.29
	 * 
	 * 
	 */
	float calcAngle(GameObject target){
	
		//Get the vector of the target and the cannon
		Vector3 targetVec = target.transform.position;
		Vector3 cannonVec = cannon.transform.FindChild("Sphere001").position;
		
		//Get the height difference, y
		float y = targetVec.y - cannonVec.y;
		
		//Set y to 0 so that the magnitude operation below only returns the length in the x plane, x
		targetVec.y = cannonVec.y = 0;
		
		//Get the length of x
		float x = (targetVec - cannonVec).magnitude;
		
		//Gravity, negated because defined as such in unity.
		float g = -Physics.gravity.y;
		
		//Inner square root in the ballistics equation
		float sqrt = (bulletVelocity*bulletVelocity*bulletVelocity*bulletVelocity) - (g * ((g * (x*x)) + (2 * y * (bulletVelocity*bulletVelocity))));
	
		//If sqrt is negative, the root below will be an imaginary number, which means that the target is out of range.
		if(sqrt < 0){
			return 0.0f;
		}
		
		//Squareroot of all evil.
		sqrt = Mathf.Sqrt(sqrt);
		

		return (Mathf.Atan(((bulletVelocity*bulletVelocity) + sqrt) / (g*x)))*Mathf.Rad2Deg;
		
		
		
	}
	
	
	/**
	 * Returns AI awake status
	 */
	public bool isAwake() {
		
		return this.awake;
		
	}
	
	
	/**
	 * Stops the AI
	 */
	public void StopAI(){
		
		StopAllCoroutines();
		awake = false;
		firstRun = true;
		
	}
	
	
	/**
	 * Starts the AI
	 */
	public void StartAI(){
		
		awake = true;
		
	}
	
	
	/**
	 * Sets difficulty
	 */
	public void setDifficulty(DIFFICULTY diff){
		
		this.diff = diff;
		
	}
	
	
	/**
	 * Recursively a difficulty modifier to offset aiming angles. 
	 */
	private float getDifficultyModifier(DIFFICULTY diff){
		
		float critMod = Random.Range (0.0f, 1.0f);
		float critCutoff = 0.4f;
		

		if(critMod <= critCutoff){
			return getDifficultyModifier(diff + 1);
		}

		
		switch (diff){
			case DIFFICULTY.VERY_EASY:
				return Random.Range(0.93f, 1.7f);
			case DIFFICULTY.EASY:
				return Random.Range(0.95f, 1.05f);
			case DIFFICULTY.NORMAL:
				return Random.Range(0.97f, 1.03f);
			case DIFFICULTY.HARD:
				return Random.Range(0.99f, 1.01f);
			case DIFFICULTY.VERY_HARD:
				return 1.0f;
			default:
				return 1.0f;
		
		}
		
	}
	
}
