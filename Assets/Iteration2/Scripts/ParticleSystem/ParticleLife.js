#pragma strict
//Currently set number
private var setLife : float;
var minLife : float;
var maxLife : float;
var tintMap : Texture2D;
var totalPixels : int = 0;
private var startTime : float;
var changeTintTime : float = 0;
var nextTintChange : float = 0;
private var x : int;
private var y : int;


function Start () {
	/*
	//4x4 test
	Debug.Log(tintMap.GetPixel(0,3));
	Debug.Log(tintMap.GetPixel(1,3));
	Debug.Log(tintMap.GetPixel(2,3));
	Debug.Log(tintMap.GetPixel(3,3));
	Debug.Log(tintMap.GetPixel(0,2));
	Debug.Log(tintMap.GetPixel(1,2));
	Debug.Log(tintMap.GetPixel(2,2));
	Debug.Log(tintMap.GetPixel(3,2));
	Debug.Log(tintMap.GetPixel(0,1));
	Debug.Log(tintMap.GetPixel(1,1));
	Debug.Log(tintMap.GetPixel(2,1));
	Debug.Log(tintMap.GetPixel(3,1));
	Debug.Log(tintMap.GetPixel(0,0));
	Debug.Log(tintMap.GetPixel(1,0));
	Debug.Log(tintMap.GetPixel(2,0));
	Debug.Log(tintMap.GetPixel(3,0));*/
	
	/*
	//2x2 test
	Debug.Log(tintMap.GetPixel(0,1));
	Debug.Log(tintMap.GetPixel(1,1));
	Debug.Log(tintMap.GetPixel(0,0));
	Debug.Log(tintMap.GetPixel(1,0));*/
	
	
	startTime = Time.time;
	if(tintMap != null){
		x = 0;
		y = tintMap.height-1;
		renderer.material.SetColor("_TintColor", tintMap.GetPixel(x,y));
		nextTintChange = startTime+changeTintTime;
	}
	setLife = Random.Range(minLife, maxLife);
}

function Update () {
	if((startTime+setLife) < Time.time){
		Destroy(gameObject);
	}
	
	if(tintMap != null && nextTintChange <= Time.time){
		//TODO GO TO NEXT PIXEL
		//Debug.Log(x);
		x++;
		if(x >= tintMap.width){
			x = 0;
			y--;
		}
		renderer.material.SetColor("_TintColor", tintMap.GetPixel(x,y));
		
		nextTintChange = changeTintTime + Time.time;
	}
}