#pragma strict
var minimumLife : float = 2.0;
var maximumLife : float = 20.0;
var componentType : PrimitiveType = PrimitiveType.Plane;
var textureParticle : Texture2D;
var textureTint : Color;
var textureTintMap : Texture2D;

var speedMultiple : float = 1;
//var minimumSpeedPerParticle : float = -100;
var maximumSpeedPerParticle : float = 100;
var delayParticle : float = 0;
var particleScale : float = 0;
var systemLifeTime : float = 0;

var limitX : boolean = false;
var limitY : boolean = false;
var limitZ : boolean = false;
var oppositeSingleDirectionX : boolean = false;
var oppositeSingleDirectionY : boolean = false;
var oppositeSingleDirectionZ : boolean = false;

var xWeight : float = 1;
var yWeight : float = 1;
var zWeight : float = 1;

var particlePerRun : int = 1;

var particleShader : String = "Particles/Additive";// = Shader.Find("Particles/Additive");
//particleObject.renderer.material.shader = Shader.Find("Particles/Additive")

//var particleSystemIdentifier : String = "Particle";
//private var particleNo : int = 0;

//var accelerateParticle : float = 0.0;

private var randomLife : float;
private var randomSpeed : Vector3;
private var particleObject : GameObject;
private var tmpX : int;
private var tmpY : int;
private var tmpZ : int;
private var timeHandler : float;
private var totalPixelsTintMap : float;
private var tintChangeTime : float;

//Color from texture WIP
/*
var heightmap : Texture2D;
private var size : Vector3 = Vector3(100, 10, 100);
private var x : int;
private var z : int;
*/



function Start () {
	timeHandler = Time.time;
	if(systemLifeTime > 0){
		systemLifeTime = systemLifeTime + Time.time;
	}
	if(maximumLife < minimumLife){
		maximumLife = minimumLife;
	}
/*if(maximumSpeedPerParticle < minimumSpeedPerParticle){
		maximumSpeedPerParticle = minimumSpeedPerParticle;
	}*/
	
	if(textureTintMap != null){
	
		//Debug.Log(textureTintMap.height);
		//Debug.Log(textureTintMap.width);
	
		totalPixelsTintMap = textureTintMap.height*(textureTintMap.width);
		tintChangeTime = maximumLife / totalPixelsTintMap;
		//Debug.Log(tintChangeTime);

	/*

		totalPixelsTintMap = 0;
		for(var i : int = 0; i < textureTintMap.height; i++){
			for(var j : int = 0; j <= textureTintMap.width; j++){
				//Debug.Log("i: " + i + " j: " + j + " Color: " + textureTintMap.GetPixel(j,i));
				totalPixelsTintMap++;
			}
		}
		tintChangeTime = maximumLife / totalPixelsTintMap;
		Debug.Log(totalPixelsTintMap);
		*/
	}

	
	
}

function Update () {
	if(Time.time > systemLifeTime && systemLifeTime > 0){
		Destroy(this);
	}
	if((Time.time - timeHandler - (delayParticle/100)) > 0){
		for(var i : int = 0; i < particlePerRun; i++){
				
			randomLife = Random.Range(minimumLife, maximumLife);
			
			tmpX = speedMultiple*(Random.Range(-maximumSpeedPerParticle, maximumSpeedPerParticle));
			tmpY = speedMultiple*(Random.Range(-maximumSpeedPerParticle, maximumSpeedPerParticle));
			tmpZ = speedMultiple*(Random.Range(-maximumSpeedPerParticle, maximumSpeedPerParticle));
			randomSpeed = Vector3(xWeight*tmpX, yWeight*tmpY, zWeight*tmpZ);
			
			particleObject = GameObject.CreatePrimitive(componentType);
			
			//particleObject.name = particleSystemIdentifier + " " + particleNo;
			//particleNo++;
			
			//Colors from a texture WIP
			/*
			x = transform.position.x / size.x*heightmap.width;
			z = transform.position.z / size.z*heightmap.height;
			transform.position.y = heightmap.GetPixel(x, z).grayscale*size.y;
			*/
			
			Destroy(particleObject.collider);
			
			particleObject.transform.position = this.transform.position;
			particleObject.transform.localScale += Vector3(particleScale, particleScale, particleScale);
			particleObject.AddComponent("Billboard");
			particleObject.AddComponent("ParticleLife");
			particleObject.GetComponent.<ParticleLife>().minLife = minimumLife;
			particleObject.GetComponent.<ParticleLife>().maxLife = maximumLife;	
			
			particleObject.AddComponent("MoveParticle");
			particleObject.GetComponent.<MoveParticle>().constantVelocity = randomSpeed;
			particleObject.GetComponent.<MoveParticle>().oneDirectionX = limitX;
			particleObject.GetComponent.<MoveParticle>().oneDirectionY = limitY;
			particleObject.GetComponent.<MoveParticle>().oneDirectionZ = limitZ;
			particleObject.GetComponent.<MoveParticle>().oppositeDirectionX = oppositeSingleDirectionX;
			particleObject.GetComponent.<MoveParticle>().oppositeDirectionY = oppositeSingleDirectionY;
			particleObject.GetComponent.<MoveParticle>().oppositeDirectionZ = oppositeSingleDirectionZ;
			//particleObject.GetComponent.<MoveParticle>().changeVelocity = accelerateParticle;
			//particleObject.GetComponent("MoveParticle").constantVelocity = Vector3(0,100,0);
			
			particleObject.renderer.material.shader = Shader.Find(particleShader);
			//particleObject.renderer.material.color = textureTint;
			particleObject.renderer.material.SetColor("_TintColor", textureTint);
			particleObject.renderer.material.mainTexture = textureParticle;
			
			particleObject.GetComponent.<ParticleLife>().changeTintTime = tintChangeTime;
			particleObject.GetComponent.<ParticleLife>().totalPixels = totalPixelsTintMap;
			particleObject.GetComponent.<ParticleLife>().tintMap = textureTintMap;
			
			timeHandler = Time.time;
		}
	}
}