using UnityEngine;
using System.Collections;

public class IT2_InputSelector : MonoBehaviour {

public enum activeControl {canon, building, menu, debug}
	
	public static activeControl activeInput;
	
	void Start () 
	{
		activeInput = activeControl.menu;
		StartCoroutine(initializeCamera());			
	}
	
	// Update is called once per frame
	void Update () 
	{	
		//Set Canon View as active
		if(Input.GetKey ("1"))
		{
			activeInput = activeControl.canon;
			//Assign Canon as cameratarget			
			//Deprecated Calls
			//Camera.main.GetComponent<CameraFollow>().camTarget = GetComponent<IT2_GameManager>().getLocalPlayer().currentCannon.transform.Find("CameraPoint");
			//Camera.main.GetComponent<IT2_PlayerCamera>().camTarget =IT2_GameManager.getLocalPlayer().currentCannon.transform.Find("CameraPoint");
			//Camera.main.GetComponent<IT2_PlayerCamera>().MoveToTarget ();
			Camera.main.GetComponent<IT2_PlayerCamera>().setNewTarget(IT2_GameManager.getLocalPlayer().currentCannon.transform.Find("CameraPoint"));
			Log ();			
		}
		
		//Set Building View as active
		if(Input.GetKey ("2"))
		{
			activeInput= activeControl.building;
			Camera.main.GetComponent<IT2_PlayerCamera>().setNewTarget(IT2_GameManager.getLocalPlayer().currentBuildingArea.transform.Find("CameraPoint"));
			Log ();
		}
		
		//Debug Control active
		if(Input.GetKey ("0"))
		{
			Camera.main.GetComponent<IT2_PlayerCamera>().isMoving = false;
			Camera.main.GetComponent<IT2_PlayerCamera>().isRotating = false;
			activeInput= activeControl.debug;
			Log ();
		}
		
		if(Input.GetKey ("escape"))
		{
			activeInput = activeControl.menu;
			Log ();
		}
	}
	
	//Coroutine for assigning the camera as soon as game starts
	IEnumerator initializeCamera()
	{		
		activeInput= activeControl.building;
		
		//Get player as soon as he is created		
		IT2_Player player = IT2_GameManager.getLocalPlayer();
		while(player == null)
		{
			player = IT2_GameManager.getLocalPlayer();
			yield return null;
		}
		
		//Get buildArea as soon as it is created
		GameObject buildArea = IT2_GameManager.getLocalPlayer().currentBuildingArea;		
		while(buildArea == null)
		{
			buildArea = IT2_GameManager.getLocalPlayer().currentBuildingArea;
			yield return null;
		}
		
		//Assign camera as soon as both while-loops are done
		Camera.main.GetComponent<IT2_PlayerCamera>().setNewTarget(IT2_GameManager.getLocalPlayer().currentBuildingArea.transform.Find("CameraPoint"));
		yield break;
	}
	
	void Log()
	{
		Debug.Log ("ActiveInput set to: " + activeInput);
	}
}
