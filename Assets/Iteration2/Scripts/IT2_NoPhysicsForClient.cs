using UnityEngine;
using System.Collections;

public class IT2_NoPhysicsForClient : MonoBehaviour {

	// Use this for initialization
	void Start () {
		// if the object is not owned by current player, disable physics for rigid bodies, to avoid jittering
		if (!networkView.isMine) {
			rigidbody.isKinematic = true;
		}
		
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
