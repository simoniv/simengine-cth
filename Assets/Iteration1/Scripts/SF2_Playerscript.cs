using UnityEngine;
using System.Collections;

public class SF2_Playerscript : MonoBehaviour {

	public NetworkPlayer owner;
	public GameObject ammo;
	
	private bool receivedTurn;
	private bool receivedPitch;
	private bool receivedShoot;
	
	private float lastTurnAngle;
	private float lastPitchAngle;
	private float lastShootPower;
	
	void Awake() {
		if (Network.isClient) {
			enabled = false;
		}
	}
	
	[RPC]
	void SetPlayer(NetworkPlayer player) {
		owner = player;
		if (player == Network.player) {
			enabled = true;
		}
	}
	
	// Use this for initialization
	void Start () {
		receivedTurn = false;
		receivedPitch = false;
		receivedShoot = false;
		lastTurnAngle = 0;
		lastPitchAngle = 0;
		lastShootPower = 0;
	}
	
	// Update is called once per frame
	void Update () {
		// Server code
		if (Network.isServer) {
			if (receivedTurn) {
				transform.Rotate(0,lastTurnAngle,0);
				receivedTurn = false;
			}
			if (receivedPitch) {
				transform.FindChild("Sphere001").transform.Rotate(lastPitchAngle,0,0);
				receivedPitch = false;
			}
			if (receivedShoot) {
				GameObject shot = (GameObject)Network.Instantiate(ammo, transform.FindChild("Sphere001").position, transform.FindChild("Sphere001").rotation, 0);
				shot.rigidbody.velocity = shot.transform.forward*lastShootPower;
				receivedShoot = false;
			}
		}
	}
	
	public void turn(float angle) {
		if (owner != null && Network.player == owner) {
			if (Network.isServer) {
				requestTurn(angle);
			} else if (Network.isClient) {
				networkView.RPC("requestTurn", RPCMode.Server, angle);
			}
		}
	}
	
	[RPC]
	void requestTurn(float angle) {
		lastTurnAngle = angle;
		receivedTurn = true;
	}
	
	public void pitch(float angle) {
		if (owner != null && Network.player == owner) {
			if (Network.isServer) {
				requestPitch(angle);
			} else if (Network.isClient) {
				networkView.RPC("requestPitch", RPCMode.Server, angle);
			}
		}
	}
	
	[RPC]
	void requestPitch(float angle) {
		lastPitchAngle = angle;
		receivedPitch = true;
	}
	
	public void shoot(float power) {
		if (owner != null && Network.player == owner) {
			if (Network.isServer) {
				requestShoot(power);
			} else if (Network.isClient) {
				networkView.RPC("requestShoot", RPCMode.Server, power);
			}
		}
	}
	
	[RPC]
	void requestShoot(float power) {
		lastShootPower = power;
		receivedShoot = true;
	}
	
	void OnSerializeNetworkView(BitStream stream, NetworkMessageInfo info) {
		if (stream.isWriting) {
			Quaternion baseRotation = transform.rotation;
			Quaternion sphereRotation = transform.FindChild("Sphere001").rotation;
			stream.Serialize(ref baseRotation);
			stream.Serialize(ref sphereRotation);
		} else {
			Quaternion recBaseRotation = Quaternion.identity;
			Quaternion recSphereRotation = Quaternion.identity;
			stream.Serialize(ref recBaseRotation);
			stream.Serialize(ref recSphereRotation);
			transform.rotation = recBaseRotation;
			transform.FindChild("Sphere001").rotation = recSphereRotation;
		}
	}
}
