using UnityEngine;
using System.Collections;

public interface SF2_iCanonShoot  {

	void shoot(float power);
	void pitch(float angle);
	void turn(float angle);

}
