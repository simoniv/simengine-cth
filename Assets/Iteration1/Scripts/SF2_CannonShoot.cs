using UnityEngine;
using System.Collections;

public class SF2_CannonShoot : MonoBehaviour //, SF2_iCanonShoot 
{
	
	public GameObject ammo;
	public float turnSpeed = 1;
	public float pitchSpeed = 1f;
	
	public float turnTarget;
	public float pitchTarget;
	
	public bool isMoving = false;
	
	private bool isTurning = false;
	private bool isPitching = false;
	
	private float precision = 1.0f;
	
	private SF2_Playerscript playerScript;
	
	
	
	
	// Use this for initialization
	void Start () {
		playerScript = GetComponent<SF2_Playerscript>();
	}
	
	// Update is called once per frame
	void Update () {
		
		//DEBUG KEYS
		if(Input.GetKey(KeyCode.RightArrow))
			turn(1f);
		if(Input.GetKey(KeyCode.LeftArrow))
			turn(-1f);
		if(Input.GetKey(KeyCode.UpArrow))
			pitch(-1f);
		if(Input.GetKey(KeyCode.DownArrow))
			pitch(1f);
		if(Input.GetKeyDown(KeyCode.Space))
			shoot(30);
		/*if(Input.GetKeyDown(KeyCode.A)){
			turnToTarget(45f);
		}
		if(Input.GetKeyDown(KeyCode.S)){
			pitchToTarget(0f);
		}
		*/
		if(!isTurning && !isPitching){
			isMoving = false;
		}
		
		
		//Turn to the target
		if((Mathf.Abs(turnTarget - transform.eulerAngles.y) >= precision) && isTurning){
			isMoving = true;
			print ("turning");
			if(Mathf.DeltaAngle(transform.eulerAngles.y, turnTarget) >= 0)
				turn(turnSpeed);
			else
				turn (-turnSpeed);			
		}else{
			isTurning = false;	
		}
		
		//Adjust pitch of the cannon
		if((Mathf.Abs(pitchTarget - currentPitch()) >= precision) && isPitching){
			isMoving = true;
			print ("pitching");
			if(Mathf.DeltaAngle(currentPitch(), pitchTarget) >= 0)
				pitch(-pitchSpeed);
			else
				pitch(pitchSpeed);
			
		}else{
			isPitching = false;	
		}

	}
	
	public void turnToTarget(float angle){
		if(angle >=0){
			turnTarget = angle%360;
		}else{
			turnTarget = 360 - Mathf.Abs(angle%360);
		}
		isTurning = true;
	}
	
	void turn(float angle){
		playerScript.turn(angle);
		//transform.Rotate(0,angle,0);
		
	}
	
	public void pitchToTarget(float angle){
		if(angle >=0){
			pitchTarget = angle%360;
		}else{
			pitchTarget = 360 - Mathf.Abs(angle%360);
		}
		isPitching = true;
		
	}
	
	void pitch(float angle){
		playerScript.pitch(angle);
		//transform.FindChild("Sphere001").transform.Rotate(angle,0,0);
		
	}
	
	public void shoot(float power){
		playerScript.shoot(power);
		//GameObject shot = (GameObject)Instantiate(ammo, transform.FindChild("Sphere001").position, transform.FindChild("Sphere001").rotation);
		//shot.rigidbody.velocity = shot.transform.forward*power;
		print ("Boom");
		GameObject.Find("CanonSound").audio.Play();
		//Play some sound. Christians responsibility
	}
	
	//get the angle from 0-360. Extremely ugly hack. I'm serious. Don't look at it
	float currentPitch(){
		//forward half
		if(Mathf.RoundToInt(transform.FindChild("Sphere001").localEulerAngles.y) == 180){
			//if 1-90
			if(transform.FindChild("Sphere001").eulerAngles.x >= 270){
				return 90 - (transform.FindChild("Sphere001").eulerAngles.x % 90);
			}
			//if 360 degrees
			else if(Mathf.RoundToInt( transform.FindChild("Sphere001").eulerAngles.x) == 0){
			//if 270 degrees	
			}
			else if(Mathf.RoundToInt( transform.FindChild("Sphere001").eulerAngles.x) == 90){
				return transform.FindChild("Sphere001").eulerAngles.x % 90 +270f;
			}
			//if 271-359
			else
				return  90 - (transform.FindChild("Sphere001").eulerAngles.x % 90) +270f;
		}		
		//back half
		if(Mathf.RoundToInt( transform.FindChild("Sphere001").eulerAngles.z) == 180){
			if(transform.FindChild("Sphere001").eulerAngles.x >= 270){
				return (transform.FindChild("Sphere001").eulerAngles.x % 90) +90f;
			}
			return (transform.FindChild("Sphere001").eulerAngles.x % 90) + 180f;
				
		}
			
		return 0;
		
		
	}
	
}
